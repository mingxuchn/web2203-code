// pages/movie/movie.js
Page({

  /**
   * 页面的初始数据
   */
  data: {  
    movie: {},  // 绑定当前选中项的电影详细信息
    isOpen: false,  // 绑定简介是否属于展开状态
    comments: [],  // 绑定评论列表
  },

  /** 点击剧照图片后执行 */
  tapImage(event){
    // 获取点击图片的data-i属性，选中图片的下标
    let i = event.target.dataset.i
    if(i==undefined){
      return; 
    }
    // 处理高清大图   去掉每个url的@后半部分
    let newurls = []
    this.data.movie.thumb.forEach(url=>{
      // url:   https://p1.meituan.net/dsfhsdkfhsdfsd.jpg@106w_106h...
      newurls.push(url.split('@')[0])
    })
    wx.previewImage({
      current: newurls[i],
      urls: newurls,
    })
  },

  /** 点击简介后，切换展开与收起状态 */
  tapIntro(){
    this.setData({
      isOpen: !this.data.isOpen
    })
  },  

  /**
   * 生命周期函数--监听页面加载
   * options 封装了上一个页面传过来的参数:   {id:111}
   */
  onLoad(options) {
    let id = options.id  // 获取选中项电影的ID
    console.log('选中的电影ID：' + id)
    // 发送https请求，加载当前id的电影的详情信息
    wx.request({
      url: 'https://api.tedu.cn/detail.php',
      method: 'GET',
      data: { id },
      success: (res)=>{
        console.log("电影详情", res)
        // 需要将res.data 存入 this.data.movie
        this.setData({
          movie: res.data  
        })
      }
    })

    // 访问云数据库，加载当前id的电影评论列表
    let db = wx.cloud.database()
    db.collection('comments').where({
      movieid: id    // 查询条件： where movieid=id
    }).skip(4).limit(4).get().then(res=>{
      console.log('电影评论列表', res)
      // 将res.data 存入 this.data.comments
      this.setData({
        comments: res.data
      })
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})