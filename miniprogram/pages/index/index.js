// index.js
Page({
  data: {
    city: '未选择', // 左上角城市名
    cid: '1',    // 绑定当前选中项的类别ID  热映:1  待映:2  经典:3
    movies: [],  // 绑定当前呈现的电影列表  {}
  },

  //当点击顶部导航选项后执行
  tapNav(event){
    let cid = event.target.dataset.id  // 获取事件源的data-id属性值
    this.setData({ cid }) 
    // 如果缓存中找到了cid对应的电影列表，直接显示
    wx.getStorage({
      key: cid, 
      success: (data)=>{  // 找到后，执行success   直接显示即可
        console.log(data)
        this.setData({movies: data.data})
      },
      fail: (err)=>{      // 没找到，执行fail
        console.warn(err)
        //如果缓存中没有，发送https请求，访问当前类别下的首页电影列表
        this.loadData(cid, 0).then(data=>{
          this.setData({
            movies: data
          })
          // 向缓存中存储data数据
          wx.setStorage({
            key: cid,
            data: data
          })
        })
      }
    })
  },

  /**
   * 封装方法，通过cid与offset加载电影列表
   * @param {number} cid  类别ID
   * @param {number} offset  起始下标
   */
  loadData(cid, offset){
    return new Promise((resolve, reject)=>{
      // 弹出等待框
      wx.showLoading({
        title: '加载中...',
      })
      // 发送请求，加载热映类别下 首页的电影列表
      wx.request({
        url: 'https://api.tedu.cn/index.php',
        method: 'GET',
        data: {cid, offset},
        success: (res)=>{  // 拿到响应，通过resolve返回 电影列表
          resolve(res.data) 
        },
        fail: (err)=>{
          reject(err)
        },
        complete: ()=>{ // 无论成功失败都执行
          wx.hideLoading()
        }
      })
    })
  },

  // 获取当前位置信息
  getLocationInfo(){
    let qqmapsdk = getApp().globalData.qqmapsdk
    qqmapsdk.reverseGeocoder({
      success: (res)=>{
        let city = res.result.address_component.city
        console.log('当前位置的逆地址解析结果：', city)
        // 将city 存入 this.data.city  在页面中使用{{city}}显示
        this.setData({city : city})
        // 将city存入globalData中
        getApp().globalData.city = city
      }
    })
  },

  onLoad(){
    // 获取当前位置信息
    this.getLocationInfo()

    // 发送请求，加载热映类别下 首页的电影列表
    this.loadData(1, 0).then(data=>{
      this.setData({movies: data})
    }).catch(err=>{
      console.warn(err)
    })
  },

  onShow(){
    // 从App.js的globalData中获取全局共享的城市
    let city = getApp().globalData.city
    this.setData({city:city})
  },

  // 监听小程序页面触底事件的钩子方法
  onReachBottom(){
    // 发送https请求，cid   offset
    let cid = this.data.cid     // 当前选中类别ID
    let offset = this.data.movies.length   // 从这个位置继续向后读
    console.log(`到底了，准备加载: cid:${cid}  offset:${offset}`)
    // 发请求
    this.loadData(cid, offset).then(data=>{
      // 判断是否到底
      if(data.length==0){
        wx.showToast({
          title: '我是有底线的',
          icon: 'error'
        })
        return;
      }
      // 更新列表   向旧数组末尾追加
      this.setData({
        movies: this.data.movies.concat(data)
      })
    })
  },

  // 当下拉刷新被触发时执行
  onPullDownRefresh(){
    console.log('下拉刷新...')
    // 发送https请求，加载当前类别下的首页，更新缓存
    this.loadData(this.data.cid, 0).then(data=>{
      // 替换列表
      this.setData({movies: data})
      // 更新缓存
      wx.setStorage({
        key: this.data.cid,
        data: data
      })
      // 取消下拉刷新
      wx.stopPullDownRefresh()
    })
  }

})
