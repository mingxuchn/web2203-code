// pages/citylist/citylist.js
const map = require('../../libs/map')
Page({

  /** 页面的初始数据 */
  data: { 
    map: map,       // 绑定所有的城市信息
    letter: 'A',    // 绑定导航字母
    city: '未选择',  // 绑定当前城市名称
    locSuccess: false, // 绑定是否定位成功
  },

  /** 点击当前定位城市文本时 */
  tapLocCity(){
    // 如果定位成功
    if(this.data.locSuccess){
      // 将this.data.city 存入 globalData
      getApp().globalData.city = this.data.city
      wx.navigateBack()
    }else {
      // 如果定位失败   点击后需要重新定位
      wx.showModal({
        title: '提示',
        content: '未授权，是否跳转到设置重新授权？',
        success: (res)=>{
          // res:  {confirm:true, cancel:false}
          if(res.confirm){ // 用户选择了确定
            // 用户同意，打开设置页引导用户重新授权
            wx.openSetting({
              success: (settingRes)=>{
                console.log('设置结果', settingRes)        
                if(settingRes.authSetting['scope.userLocation']){
                  // 用户重新赋予了定位权限, 重新定位
                  this.getLocationInfo()
                }
              }
            })
          }
        }
      })
    }
  },

  /** 点击右侧导航字母 */
  tapLetter(event){
    let l = event.target.dataset.l
    this.setData({
      letter: l
    })
  },

  /** 点击城市列表项文本时触发 */
  tapCity(event){
    let city = event.target.dataset.city
    console.log(city)
    if(city == undefined) return;
    // 将城市名称存入App.js中的globalData里
    getApp().globalData.city = city
    wx.navigateBack()
  },

  // 获取当前位置信息
  getLocationInfo(){
    let QQMapWX = require('../../libs/qqmap-wx-jssdk')
    let qqmapsdk = new QQMapWX({
      key: 'A7CBZ-FZ73U-PUPV7-BINEG-ICD57-KAB6J'
    })
    qqmapsdk.reverseGeocoder({
      success: (res)=>{
        let city = res.result.address_component.city
        console.log('当前位置的逆地址解析结果：', city)
        // 将city 存入 this.data.city  在页面中使用{{city}}显示
        this.setData({
          city : city,
          locSuccess: true // 记录一下定位成功
        })
      },
      fail: (err)=>{
        console.warn(err)
        this.setData({
          city: '定位失败，点击重试',
          locSuccess: false // 记录一下定位失败
        })
      }
    })
  },

  /** 生命周期函数--监听页面加载 */
  onLoad(options) {
    console.log(map)
    // 加载当前位置
    this.getLocationInfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})