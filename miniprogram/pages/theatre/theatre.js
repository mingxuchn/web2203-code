// pages/theatre/theatre.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    city: '未选择',  // 绑定当前城市名称
    theatreList: [],  // 绑定当前影院列表
  },
  
  /** 点击电影院列表项 */
  tapTheatre(event){
    // event.currentTarget描述绑定事件的那个组件
    let i = event.currentTarget.dataset.i  
    let t = this.data.theatreList[i]  // 当前选中的电影院对象
    wx.openLocation({
      latitude: t.location.lat,
      longitude: t.location.lng,
      name: t.title,
      address: t.address,
      scale: 12
    })     
  },

  /**生命周期函数--监听页面加载*/
  onLoad: function (options) {

  },

  onShow(){
    // 从globalData中加载city，更新左上角
    let city = getApp().globalData.city
    this.setData({city})
    // 查询city城市中的所有电影院  查腾讯位置服务
    let qqmapsdk = getApp().globalData.qqmapsdk
    qqmapsdk.search({
      keyword: '影院',
      region: city,
      page_size: 20,
      success: (res)=>{
        console.log('影院列表：', res)
        // 处理一下res.data，整理距离单位
        res.data.forEach(item=>{
          // 为每一个影院对象添加一个新属性
          item._disstr = 
            (item._distance/1000).toFixed(2)
        })
        // 将res.data存入this.data.theatreList
        this.setData({
          theatreList: res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})