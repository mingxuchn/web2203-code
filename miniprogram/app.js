// app.js  小程序初次加载时执行，并创建App的对象
// App对象全局唯一。
let QQMapWX = require('./libs/qqmap-wx-jssdk')
let qqmapsdk = new QQMapWX({
  key: 'A7CBZ-FZ73U-PUPV7-BINEG-ICD57-KAB6J'
})
App({
  // 自定义一个globalData属性用于存储一些全局共享的数据
  // 在页面中如何访问这些数据呢？如下：
  // 页面.js中：
  // getApp().globalData.city
  globalData: {
    qqmapsdk,  //  存储qqmapsdk，全局共享腾讯位置服务
    city: '未选择', // 存储全局共享的城市名称
  },

  // 应用初次启动时执行
  onLaunch(){
    // 初始化云服务
    wx.cloud.init({
      env: 'cloud2012-9gl0qd6g8dc72b1c'
    })
  }
})
