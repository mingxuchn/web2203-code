Component({
  /** 组件的属性列表 */
  properties: {
    "round" : {
      type: Boolean,
      value: false
    },
    
    "color" : {
      type: String,
      value: 'crimson'
    },
    // 为按钮设计一个自定义属性：title
    // <my-button title="重新赋值"></my-button>
    "title" : {
      type: String,
      value: '按钮文本'   // 属性默认值：按钮文本
    }
  },

  /** 组件的初始数据 */
  data: {
    last: 0,   // 用于保存上一次点击的时间戳
  },
  /** 组件的方法列表 */
  methods: {
    tapview (){ // 点击view 就会执行
      console.log('点击了mybutton组件中的view')
      // 判断当前这一次点击是否属于双击
      let now = new Date().getTime() // 当前时间
      let last = this.data.last // 上一次时间
      if(now-last<350) { // 达到了触发双击的条件
        // 通知组件使用者，doubletap事件触发了
        this.triggerEvent('doubletap')
        now = 0
      }
      this.data.last = now // 重置时间戳
    }
  }
})
